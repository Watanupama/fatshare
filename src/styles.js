'use strict';
var React = require('react-native');

var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({
formInput : {
    width:300, 
    flexDirection:'row',
    borderBottomWidth:1,
    borderBottomColor:'#fff',
    
},
inputIcons:{   
        marginTop:15,
        position:'absolute'
},
formText:{
    marginLeft:25,
    width:250,
    color:'#fff'
},
mainForm : {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems:'center'
},
top40 : {
    marginTop:80
},
loginFormMargin:{
    marginTop:40
},
forgotLabel : {
    flexDirection:'row',
    marginTop:20,
    alignItems:'stretch'
}

});