import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
// import Home from '../home/Home.js'
// import About from '../about/About.js'
import Landing from './screens/Landing';
import Categories from './screens/Categories';
import Login from './screens/Login'

const Routes = () => (
   <Router showNavigationBar={false}>
            <stack>
                  <Scene key = "categories" component = {Categories} hideNavBar={true}/>
                  <Scene key = "login" component = {Login} initial = {true} hideNavBar={true}/>
                  {/* <Scene key = "landing" component = {Landing} initial = {true} hideNavBar={true}/> */}
            </stack>
   </Router>
)
export default Routes