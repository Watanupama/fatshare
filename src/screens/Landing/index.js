import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    ScrollView,
    Text,
    View,
    Button,
    Image,
    StyleSheet,
    ImageBackground
} from 'react-native';

let styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    loginForm: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignItems:'center'
    }
    ,logo:{
       backgroundColor:'transparent',
       marginTop:50,
       width: 200,
       resizeMode: 'contain' 
    },
    offering:{
        marginTop:30,
        width: 250,
        height:130,
       resizeMode: 'contain' 
    },
    seeking:{
        width: 250,
        height:130,
       resizeMode: 'contain' 
    },
    helpme:{
        width: 180,
        resizeMode: 'contain',
        marginTop:80
    }
});
export default class Landing extends Component {
   
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../../images/landing_background.png')} style={styles.backgroundImage} />
                <View style={styles.loginForm}>
                    <Image source={require('../../../images/logo.png')} style={styles.logo} />
                    <Image source={require('../../../images/offering.png')} style={styles.offering} onPress = {Actions.categories()}/>
                    <Image source={require('../../../images/seeking.png')} style={styles.seeking} />
                    <Image source={require('../../../images/saveme.png')} style={styles.helpme} />
                </View>
                
            </View>
                )
    }
}
