import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from 'react-native-checkbox';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    StyleSheet,
    Image
} from 'react-native';
var s = require('../../styles');
let styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    mainForm : {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignItems:'center'
    },
    logo:{
        backgroundColor:'transparent',
        marginTop:50,
        width: 200,
        resizeMode: 'contain' 
     },
     searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    searchIcon: {
        padding: 10,
    },
    input: {
        flex: 1,
        color: '#424242',
    }
});
export default class Login extends Component {

    state = {
        email: '',
            password: '',
            isLoggingIn: false,
            message: ''
        }
        _userLogin = () => { 

            this.setState({isLoggingIn: true, message:''});
            var params = {
                username: this.state.email,
                password: this.state.password,
                grant_type: 'password'
            };

            var formBody = [];
            for (var property in params) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(params[property]);
            formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            var proceed = false;
            fetch("https://”+Environment.CLIENT_API+”/oauth/token", {
                method: "POST", 
                headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: formBody
            })
            .then((response) => response.json())
            .then((response) => {
                if (response.error) this.setState({message: response.message});
                else proceed = true;
            })
            .then(() => {
                this.setState({isLoggingIn: false})
                if (proceed) this.props.onLoginPress();
            })
            .done();
        }
        
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../../images/landing_background.png')} style={styles.backgroundImage} />
                <View style={s.mainForm}>
                <Image source={require('../../../images/logo.png')} style={styles.logo} />

                    <View style={[s.formInput,s.loginFormMargin]}>
                        <Icon name="envelope-o" size={20} style={s.inputIcons}/>
                        <TextInput placeholder='Email' placeholderTextColor="white" underlineColorAndroid="transparent" style={s.formText} onChangeText={(text) => this.setState({email})}/>
                    </View>

                    <View style={s.formInput}>
                        <Icon name="lock" size={20} style={s.inputIcons}/>
                        <TextInput placeholder='Password' placeholderTextColor="white" underlineColorAndroid="transparent" style={s.formText} onChangeText={(text) => this.setState({email})}/>
                    </View>

                    <View style={s.forgotLabel}>
                        <CheckBox label="Remember Me" labelStyle={{color:'#fff'}} />
                        <Text style={{color:'#fff'}}>
                            Forgot Password
                            
                        </Text>
                    </View>
                    <View>
                        <Button title="Login" color="#000"/>
                    </View>
               
                </View>
                
            </View>
            // <ScrollView style={{padding: 20}}>
            // <Text 
            //     style={{fontSize: 27}}>
            //     Login
            // </Text>
            // <TextInput placeholder='Email' onChangeText={(text) => this.setState({email})}/>
            // <TextInput placeholder='Password' onChangeText={(text) => this.setState({password})} />
            // <View style={{margin:7}} />
            // {!!this.state.message && (
            //     <Text
            //             style={{fontSize: 14, color: 'red', padding: 5}}>
            //             {this.state.message}
            //         </Text>
            //     )}
            // <Button 
            //     disabled={this.state.isLoggingIn||!this.state.email||!this.state.password}
            //     onPress={this._userLogin}
            //     title="Submit"
            //     />
            // </ScrollView>
            )
    }
}